package com.nmgslx.examples.example_testng;

import org.testng.Reporter;

public class Logger {
	private Object obj = null;

	public Logger(Object obj) {
		this.setObj(obj);
	}

	private String formatLog(Object msg) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[{");
		buffer.append(Thread.currentThread().hashCode());
		buffer.append("}]\t");
		buffer.append(msg.toString());
		return buffer.toString();
	}

	public void info(Object msg) {
		Reporter.log(formatLog(msg), true);
	}

	public void error(Object msg) {
		Reporter.log(formatLogErr(msg), true);
	}

	private String formatLogErr(Object msg) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<font color=\"red\">");
		buffer.append("[{");
		buffer.append(Thread.currentThread().hashCode());
		buffer.append("}]\t");
		buffer.append(msg.toString());
		buffer.append("</font>");
		return buffer.toString();
	}

	public void warn(Object msg) {
		Reporter.log(formatLog(msg), true);
	}

	public static Logger getLogger(Object obj) {
		return new Logger(obj);
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
}
