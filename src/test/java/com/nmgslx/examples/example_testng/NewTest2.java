package com.nmgslx.examples.example_testng;

import org.testng.annotations.*;

public class NewTest2 {
  @Test
  public void test21() {
	  Utils.logger.info("test21");
  }
  
  @Test(priority = 1)
  public void test22() {
	  Utils.logger.info("test22");
  }

  @Test
  public void test23() {
	  Utils.logger.info("test23");
  }

  @BeforeMethod
  public void beforeMethod1() {
	  Utils.logger.info("test2-beforeMethod1");
  }
  
  @BeforeMethod
  public void beforeMethod2() {
	  Utils.logger.info("test2-beforeMethod2");
  }

  @AfterMethod
  public void afterMethod() {
	  Utils.logger.info("test2-afterMethod");
  }

  @BeforeClass
  public void beforeClass() {
	  Utils.logger.info("test2-beforeClass");
  }
  @AfterClass
  public void afterClass() {
	  Utils.logger.info("test2-afterClass");
  }

  @BeforeTest
  public void beforeTest() {
	  Utils.logger.info("test2-beforeTest");
  }
  @AfterTest
  public void afterTest() {
	  Utils.logger.info("test2-afterTest");
  }

  @BeforeSuite
  public void beforeSuite() {
	  Utils.logger.info("test2-beforeSuite");
  }

  @AfterSuite
  public void afterSuite() {
	  Utils.logger.info("test2-afterSuite");
  }
  
}
