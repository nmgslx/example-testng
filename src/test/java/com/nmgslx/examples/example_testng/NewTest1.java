package com.nmgslx.examples.example_testng;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.BeforeSuite;

public class NewTest1 {
  @Test(dependsOnMethods={"test12","test13"})
  public void test11() {
	  Utils.logger.info("[test11]");
  }
  
  @Test
  @Parameters({"param1","param2","param3"})
  public void test12(String param1, @Optional("param2-default") String param2,
		  @Optional("param3-default") String param3) {
	  Utils.logger.info("[test12]");
	  Utils.logger.info(String.format("p1=%s,p2=%s,p3=%s",param1,param2,param3));
  }

  @Test
  public void test13() {
	  Utils.logger.info("[test13]");
	  double x = Math.random();
	  Utils.logger.info("x="+x);
	  Assert.assertTrue(x>0.5);
  }

  @BeforeMethod
  public void beforeMethod() {
	  Utils.logger.info("test1 beforeMethod");
  }

  @BeforeClass
  public void beforeClass() {
	  Utils.logger.info("test1 beforeClass");
  }

  @BeforeTest
  public void beforeTest() {
	  Utils.logger.info("test1 beforeTest");
  }

  @BeforeSuite
  public void beforeSuite() {
	  Utils.logger.info("test1 beforeSuite");
  }

}
